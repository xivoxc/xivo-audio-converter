FROM python:3.7-alpine

# Add dependencies
RUN apk add --no-cache lame \
# Bring in gettext so we can get `envsubst`, then throw
# the rest away. To do this, we need to install `gettext`
# then move `envsubst` out of the way so `gettext` can
# be deleted completely, then move `envsubst` back.
    && apk add --no-cache --virtual .gettext gettext \
    && mv /usr/bin/envsubst /tmp/ \
    \
    && runDeps="$( \
        scanelf --needed --nobanner /tmp/envsubst \
            | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
            | sort -u \
            | xargs -r apk info --installed \
            | sort -u \
    )" \
    && apk add --no-cache $runDeps \
    && apk del .gettext \
    && mv /tmp/envsubst /usr/local/bin/

WORKDIR /usr/src/xivo_audio_converter
RUN mkdir -p xivo_audio_converter bin

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY xivo_audio_converter/ xivo_audio_converter/
COPY bin/ bin/
COPY etc/ /etc/
COPY docker-entrypoint.d/ /docker-entrypoint.d/
COPY setup.py .
RUN python setup.py install

COPY docker-entrypoint.sh /
RUN chmod 755 /docker-entrypoint.sh

ARG TARGET_VERSION
LABEL version=${TARGET_VERSION}

ENV SRC_DIR="/var/spool/recording-wav-files"
ENV DST_DIR="/var/spool/recording-server"
ENV CONFIG_FILE="/etc/xivo-audio-converter.conf"
ENV PRESET="standard"
ENV OUTPUT_FORMAT="mp3"

STOPSIGNAL SIGKILL

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["xivo-audio-converter"]