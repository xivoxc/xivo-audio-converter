# Présentation

Démon de conversion de WAV en mp3 ou un autre format.

Le daemon surveille par défaut, le dossier `/var/spool/recording-wav-files/`, lorsqu'un nouveau enregistrement (au format wav) est poussé dans le dossier, le fichier est convertit en mp3 puis il est mis dans le dossier (par défaut) `/var/spool/recording-server/`

# Installation / Lancement

1. Ajouter le fichier (ou le contenu de) docker-xivocc.override.yml dans votre xivocc
2. Executer la commande `xivocc-dcomp up -d`

# Configuration du container 

Les valeurs par défaut sont :

- SRC_DIR "/var/spool/recording-wav-files"
- DST_DIR "/var/spool/recording-server"
- CONFIG_FILE "/etc/xivo-audio-converter.conf"
- PRESET "standard"
- OUTPUT_FORMAT "mp3"

Vous pouvez utiliser les variables d'environnement de docker pour écraser les valeurs par défaut ou utiliser votre propre fichier de configuration.
La variable PRESET peut avoir comme valeur :

- medium
- standard
- extreme
- insane

Pour plus d'information sur les possibilités de conversion, regardez : https://lame.sourceforge.io/
# Traitement des anciens fichiers
Il est possible de retraiter les anciens fichiers et de les convertir en MP3, en les déplaçant dans le répertoire surveillé.

Afin de pas charger la machine, il est conseillé de convertir par petit groupe, en sélectionnant par exemple les fichiers wav de plus de X jours. Pour cela, il modifier le paramètre **-mtime**
  * `find /var/spool/recording-server/<nomduxivo>/ -maxdepth 1 -name *.wav -mtime +364 -type f -exec mv "{}" /var/spool/recording-wav-files/<nomduxivo>/`

Même convertis en mp3, les anciens fichiers pourront être téléchargés depuis l'interface de recherche d'enregistrement.

**ATTENTION, il faut retraiter les fichiers par petit groupe afin de pas charger la VM**

# Configuration Multi XIVO/GW

Par exemple, si j'ai :
* xivo
* xivogw1
* xivogw2

Il va falloir configurer **3** xivo-audio-converter :
Pour chaque XiVO (dans la suite les exemples sont donnés pour le xivo *xivogw1*) :
1. Créer un fichier `/etc/xivo-audio-converter`-**nom du xivo**`.conf` (e.g. `/etc/xivo-audio-converter-xivogw1.conf`),
2. Copier le fichier `/etc/init.d/xivo-audio-converter` en `/etc/init.d/xivo-audio-converter`**-nom du xivo** (e.g. `/etc/init.d/xivo-audio-converter-xivogw1`)
3. Editez ce fichier et renseignez :
  * `DAEMONNAME` avec **le nom du XiVO** (e.g. xivogw1)
  * et `CONF_FILE_PATH` avec *le chemin vers le fichier de configuration de l'étape 1.* (e.g.
    `/etc/xivo-audio-converter-xivogw1.conf`).
4. Enfin, il faut éditer le fichier de configuration `/etc/xivo-audio-converter`-**nom du xivo**`.conf` :
  * `src_dir` : répertoire contenant les fichiers WAV d'origine (peuvent se situer dans des sous-répertoires)
  * `dst_dir` : répertoire de destination des fichiers compressés
  * **Attention**: Le propriétaire de ces répertoires **DOIT** être daemon
  * Les autres paramètres sont :
    * compressor: binaire à utiliser pour la compression, si le binaire n'est pas dans le path le chemin complet, par défaut 'lame'
    * compressor_params: paramétres à passer au compresseur, par défaut '-preset standard'
    * dst_file_extension: extension des fichiers compressés, par défaut 'mp3'
5. La dernière étape est de configurer la rotation de log pour chaque xivo. Pour cela, éditer le fichier
   `/etc/logrotate.d/xivo-audio-converter` et rajouter une ligne par xivo.
  
   Dans notre exemple :
  
   ```
   /var/log/xivo-audio-converter/xivo-audio-converter-xivo.log
   /var/log/xivo-audio-converter/xivo-audio-converter-xivogw1.log
   /var/log/xivo-audio-converter/xivo-audio-converter-xivogw2.log {
       daily
       ...
   ```

# Log 

Les logs se trouvent dans :  
`/var/log/xivo-audio-converter/xivo-audio-converter`**-nom du xivo**`.log`


# ATTENTION

* Les sous-répertoires présents dans le src_dir doivent exister dans dst_dir
* Leur propriétaire doit être daemon
* Dans le cas d'une utilisation combinée avec le serveur d'enregistrement :
  * chaque fois que l'on ajoute une gateway, ajouter également un répertoire du même nom dans dst_dir (propriétaire : daemon) 
