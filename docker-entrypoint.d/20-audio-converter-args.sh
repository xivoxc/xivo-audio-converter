#!/bin/sh

set -e

ME=$(basename $0)

entrypoint_log() {
    if [ -z "${XIVO_ENTRYPOINT_QUIET_LOGS:-}" ]; then
        echo "$@"
    fi
}

mkdir -p /var/log/xivo-audio-converter/ $SRC_DIR $DST_DIR
touch /var/log/xivo-audio-converter/xivo-audio-converter.log

if [ -s $CONFIG_FILE ]
then
    exit 0
else 
    entrypoint_log "$ME: ERROR: $CONFIG_FILE is empty, can't start"
    exit 1
fi