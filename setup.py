#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from distutils.core import setup

setup(
    name='xivo-audio-converter',
    version='1.4',
    description='Audio conversion daemon',
    maintainer='Avencall',
    maintainer_email='egratas@xivo.solutions',

    packages=['xivo_audio_converter'],
    scripts=['bin/xivo-audio-converter']
)
